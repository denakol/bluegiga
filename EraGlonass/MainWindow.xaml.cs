﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using LibraryNTC;

namespace EraGlonass
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Progress<String> progress = new Progress<string>(); 
        private readonly Reciver reciver = new Reciver();

        public MainWindow()
        {
            InitializeComponent();
            progress.ProgressChanged += Log;
        }

        private void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            var window = new Setting_Window();
            window.ShowDialog();
        }


  

        private async void btn_Receive_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.PortName.Length != 0)
            {
                var countBytes = Convert.ToInt32(txt_numberOfBytes.Text);
                await reciver.Receive(progress, countBytes);
            }
            else
            {
                MessageBox.Show("Задайте имя порта");
            }
        }

        private void Log(Object obj, String message)
        {
            txt_Log.Text += message + "\n";
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await reciver.ReceiveWrite(progress, txt_Message.Text);
        }
       
    
    }
}
