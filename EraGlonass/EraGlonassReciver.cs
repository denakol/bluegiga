﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EraGlonass
{
    public class Reciver
    {
        public SerialPort Initilizate(IProgress<String> progress)
        {
            var port = new SerialPort
            {
                BaudRate = Properties.Settings.Default.BaudRate,
                Parity = (Parity)Enum.Parse(typeof(Parity), Properties.Settings.Default.Parity),
                StopBits = (StopBits)Enum.Parse(typeof(StopBits), Properties.Settings.Default.StopBits),
                PortName = Properties.Settings.Default.PortName
            };
            progress.Report("Открытие порта");
            try
            {
                port.Open();
            }
            catch (System.UnauthorizedAccessException ex)
            {
                progress.Report("Отказ в доступе к порту или для текущего процесса или другого процессав системе уже открыт заданный порт COM.");
                return null;
            }
            catch (System.IO.IOException ex)
            {
                progress.Report(String.Format("Порт находится в недействительном состоянии. {0}", ex.Message));
                return null;
            }

            progress.Report("Порт открыт");
            return port;


        }
        public async Task Receive(IProgress<String> progress, Int32 numberOfBytes)
        {
            var port = Initilizate(progress);
            if (port == null)
            {
                return;
            }
            progress.Report("Чтение сообщений");
            try
            {
                using (port)
                {
                    await Read(port, progress, numberOfBytes);
                }
            }
            catch (System.TimeoutException ex)
            {
                progress.Report("Отсутствуют байты, доступные для чтения.");
                return;
            }
            progress.Report("Завершено");

        }

        public async Task ReceiveWrite(IProgress<String> progress, String Buf)
        {
            var port = Initilizate(progress);
            if (port == null)
            {
                return;
            }
            progress.Report("Запись");
            try
            {
                using (port)
                {
                    await Write(port, progress, Buf);
                }
            }
            catch (System.TimeoutException ex)
            {
                progress.Report("Отсутствуют байты, доступные для записи.");
                return;
            }
            progress.Report("Завершено");
        }

        private async Task Write(SerialPort port, IProgress<string> progress, string buf)
        {
            var messages = buf.Split(new Char[] { '/' });
            var messagesInt = messages.Select(x => Convert.ToInt32(x));
            var bytes = messagesInt.Select(x => BitConverter.GetBytes(x))
                                   .SelectMany(x => x)
                                   .ToArray();
            await port.BaseStream.WriteAsync(bytes, 0, bytes.Count());
        }


        private async Task Read(SerialPort port, IProgress<String> progress, Int32 numberOfBytes)
        {

            Int16 endMessage = Properties.Settings.Default.StopReceive;
            var buf = new byte[numberOfBytes];
            progress.Report("Чтение");
            var task = port.BaseStream.ReadAsync(buf, 0, numberOfBytes);
            if (task != await Task.WhenAny(task, Task.Delay(5000)))
            {
                throw new System.TimeoutException();
            }
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < numberOfBytes; i++)
            {
                str.Append(buf[i].ToString());
            }
            progress.Report(str.ToString());

        }

    }
}
