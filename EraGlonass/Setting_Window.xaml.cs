﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EraGlonass
{
    /// <summary>
    /// Логика взаимодействия для Setting_Window.xaml
    /// </summary>
    public partial class Setting_Window : Window
    {

        string[] baudeRate = {  "2400",
                                "4800",
                                "9600",
                                "19200",
                                "38400",
                                "57600",
                                "115200"};
        public Setting_Window()
        {
            InitializeComponent();

            InitializeComponent();

            cbBaudeRate.ItemsSource = baudeRate;
            cbBaudeRate.SelectedItem =Properties.Settings.Default.BaudRate.ToString();

            cbParity.ItemsSource = Enum.GetValues(typeof(Parity));
            cbParity.Text = Properties.Settings.Default.Parity;

            cbStopBits.ItemsSource = Enum.GetValues(typeof(StopBits));
            cbStopBits.Text = Properties.Settings.Default.StopBits;

            cbPortName.ItemsSource = SerialPort.GetPortNames();
            cbPortName.Text = Properties.Settings.Default.PortName;
            MessageReceive.Text = Properties.Settings.Default.Invite.ToString();
            MessageStop.Text = Properties.Settings.Default.StopReceive.ToString();
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.BaudRate = Convert.ToInt32(cbBaudeRate.SelectedItem);
            Properties.Settings.Default.Parity = cbParity.SelectedItem.ToString();
            Properties.Settings.Default.StopBits = cbStopBits.SelectedItem.ToString();
            if (SerialPort.GetPortNames().Any(x=> x== cbPortName.Text))
            Properties.Settings.Default.PortName = cbPortName.Text;
            else
            {
                Properties.Settings.Default.PortName = "";
            }
            
            Properties.Settings.Default.Invite = Int16.Parse(MessageReceive.Text);
            Properties.Settings.Default.StopReceive = Int16.Parse(MessageStop.Text); 
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

   
    }
}
