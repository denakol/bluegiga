﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace LibraryNTC
{
     public class SerizableByteArray<T> 
    {
        public static T Deserialize(byte[] serializedAsBytes)
        {
            var stream = new MemoryStream();
            var formatter = new BinaryFormatter();
            stream.Write(serializedAsBytes, 0, serializedAsBytes.Length);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
        public static byte[] Serialize(T obj)
        {
            var formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

      
    }
}
